function generator() {

	$("#select1 dd").click(function() {
		$(this).addClass("selected").siblings().removeClass("selected");
		if($(this).hasClass("select-all")) {
			$("#selectA").remove();
		} else {
			var copyThisA = $(this).clone();
			if($("#selectA").length > 0) {
				$("#selectA a").html($(this).text());
			} else {
				$(".select-result dl").append(copyThisA.attr("id", "selectA"));
			}
		}
		getQueryParams();
	});

	$("#select2 dd").click(function() {
		$(this).addClass("selected").siblings().removeClass("selected");
		if($(this).hasClass("select-all")) {
			$("#selectB").remove();
		} else {
			var copyThisB = $(this).clone();
			if($("#selectB").length > 0) {
				$("#selectB a").html($(this).text());
			} else {
				$(".select-result dl").append(copyThisB.attr("id", "selectB"));
			}
		}
		getQueryParams();
	});

	$("#select3 dd").click(function() {
		$(this).addClass("selected").siblings().removeClass("selected");
		if($(this).hasClass("select-all")) {
			$("#selectC").remove();
		} else {
			var copyThisC = $(this).clone();
			if($("#selectC").length > 0) {
				$("#selectC a").html($(this).text());
			} else {
				$(".select-result dl").append(copyThisC.attr("id", "selectC"));
			}
		}
		getQueryParams();
	});

	$("#selectA").on("click", function() {
		$(this).remove();
		$("#select1 .select-all").addClass("selected").siblings().removeClass("selected");
	});

	$("#selectB").on("click", function() {
		$(this).remove();
		$("#select2 .select-all").addClass("selected").siblings().removeClass("selected");
	});

	$("#selectC").on("click", function() {
		$(this).remove();
		$("#select3 .select-all").addClass("selected").siblings().removeClass("selected");
	});

	$(".select dd").on("click", function() {
		if($(".select-result dd").length > 1) {
			$(".select-no").hide();
		} else {
			$(".select-no").show();
		}
	});
}

function getQueryParams() {
	var area = $("#select1 .selected a").text();
	if(area == '全部') {
		area = 'all';
	}
	var year = $("#select2 .selected a").text();
	if(year == '全部') {
		year = 'all';
	}

	var type = $("#select3 .selected a").text();

	if(type == '全部') {
		type = 'all';
	}

	var url = base_url + 'client/vod/category/3/' + area + '/' + year + '/' + type;
	getList(url);
}

function getList(url) {
	mui.ajax(url, {
		dataType: 'json',
		type: 'get',
		timeout: 20000,
		success: function(data) {
			vodList = data.videoList;
			mui('#pullrefresh').pullRefresh().pullupLoading();
		},
		error: function(error) {
			alert("无数据！");
		}
	});
}