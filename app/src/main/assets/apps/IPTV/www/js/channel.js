mui.init({
	swipeBack: true //启用右滑关闭功能
});

mui.plusReady(function(){
	
	var channelPages = ['channel/tv.html','channel/movie.html','channel/variety.html','channel/news.html'];
	
	for (var i = 0; i < 4; i++) {
		openChannelList('channelList' + i, channelPages[i]);
	}
	
	// 关闭应用
	quit();
});

// 打开频道列表
function openChannelList(id, page) {
	var channelBtn = document.getElementById(id);
	channelBtn.addEventListener('click', function() {
		var channelList = plus.webview.getWebviewById(page);
		if(!channelList) {
			channelList = plus.webview.create(page, page, {}, {});
		}
		plus.webview.show(channelList, 'slide-in-right');
	})
}

// 退出APP
function quit() {
	var backButtonPress = 0;
	mui.back = function(event) {
		backButtonPress++;
		if (backButtonPress > 1) {
			plus.runtime.quit();
		} else {
			plus.nativeUI.toast('再按一次退出应用');
		}
		setTimeout(function() {
			backButtonPress = 0;
		}, 1000);
		return false;
	};
}