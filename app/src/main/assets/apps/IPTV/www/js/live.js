var list1;
var list2;
var list3;
var list4;
mui.init({
	swipeBack: false
});

mui.plusReady(function() {

	mui('.mui-scroll-wrapper').scroll({
		indicators: true //是否显示滚动条
	});

	getChannelList();

	var item1 = document.getElementById('item1mobile');
	var item2 = document.getElementById('item2mobile');
	var item3 = document.getElementById('item3mobile');
	var item4 = document.getElementById('item4mobile');

	setTimeout(function() {
		var html = '<ul class="mui-table-view">';
		for(var i = 0; i < list1.length; i++) {
			html += '<li class="mui-table-view-cell mui-media"><div style="width: 90%;float: left;"><a id="list1-' + i + '" href="javascript:;"><img class="mui-media-object mui-pull-left" src="' + base_img_url + list1[i].icon + '"><div class="mui-media-body">' + list1[i].title + '<p class="mui-ellipsis"></p></div></a></div><div style="width: 8%;float: right;"><span class="mui-icon iconfont icon-bofang"></span></div></li>';
		}
		html += '</ul>';
		item1.querySelector('.mui-scroll').innerHTML = html;
		for(var i = 0; i < list1.length; i++) {
			openPage('list1-' + i, list1[i].playURL, list1[i].channelId, list1[i].title, list1[i].bindChannel, list1[i].multicastUrl);
		}
	}, 500);

	document.getElementById('slider').addEventListener('slide', function(e) {
		if(e.detail.slideNumber === 1) {
			if(item2.querySelector('.mui-loading')) {
				setTimeout(function() {
					var html = '<ul class="mui-table-view">';
					for(var i = 0; i < list2.length; i++) {
						html += '<li class="mui-table-view-cell mui-media"><div style="width: 90%;float: left;"><a id="list2-' + i + '" href="javascript:;"><img class="mui-media-object mui-pull-left" src="' + base_img_url + list2[i].icon + '"><div class="mui-media-body">' + list2[i].title + '<p class="mui-ellipsis"></p></div></a></div><div style="width: 8%;float: right;"><span class="mui-icon iconfont icon-bofang"></span></div></li>';
					}
					html += '</ul>';
					item2.querySelector('.mui-scroll').innerHTML = html;
					for(var i = 0; i < list2.length; i++) {
						openPage('list2-' + i, list2[i].playURL, list2[i].channelId, list2[i].title, list2[i].bindChannel, list2[i].multicastUrl);
					}
				}, 500);
			}
		} else if(e.detail.slideNumber === 2) {
			if(item3.querySelector('.mui-loading')) {
				setTimeout(function() {
					var html = '<ul class="mui-table-view">';
					for(var i = 0; i < list3.length; i++) {
						html += '<li class="mui-table-view-cell mui-media"><div style="width: 90%;float: left;"><a id="list3-' + i + '" href="javascript:;"><img class="mui-media-object mui-pull-left" src="' + base_img_url + list3[i].icon + '"><div class="mui-media-body">' + list3[i].title + '<p class="mui-ellipsis"></p></div></a></div><div style="width: 8%;float: right;"><span class="mui-icon iconfont icon-bofang"></span></div></li>';
					}
					html += '</ul>';
					item3.querySelector('.mui-scroll').innerHTML = html;
					for(var i = 0; i < list3.length; i++) {
						openPage('list3-' + i, list3[i].playURL, list3[i].channelId, list3[i].title, list3[i].bindChannel, list3[i].multicastUrl);
					}
				}, 500);
			}
		} else if(e.detail.slideNumber === 3) {
			if(item4.querySelector('.mui-loading')) {
				setTimeout(function() {
					var html = '<ul class="mui-table-view">';
					for(var i = 0; i < list4.length; i++) {
						html += '<li class="mui-table-view-cell mui-media"><div style="width: 90%;float: left;"><a id="list4-' + i + '" href="javascript:;"><img class="mui-media-object mui-pull-left" src="' + base_img_url + list4[i].icon + '"><div class="mui-media-body">' + list4[i].title + '<p class="mui-ellipsis"></p></div></a></div><div style="width: 8%;float: right;"><span class="mui-icon iconfont icon-bofang"></span></div></li>';
					}
					html += '</ul>';
					item4.querySelector('.mui-scroll').innerHTML = html;
					for(var i = 0; i < list4.length; i++) {
						openPage('list4-' + i, list4[i].playURL, list4[i].channelId, list4[i].title, list4[i].bindChannel, list4[i].multicastUrl);
					}
				}, 500);
			}
		}
	});
	var sliderSegmentedControl = document.getElementById('sliderSegmentedControl');
	mui('.mui-input-group').on('change', 'input', function() {
		if(this.checked) {
			sliderSegmentedControl.className = 'mui-slider-indicator mui-segmented-control mui-segmented-control-inverted mui-segmented-control-' + this.value;
			//force repaint
			sliderProgressBar.setAttribute('style', sliderProgressBar.getAttribute('style'));
		}
	});

	// 关闭应用
	quit();
});

/**
 * 获取直播频道列表
 */
function getChannelList() {
	mui.ajax(LIVE_CHANNEL, {
		async: true,
		dataType: 'json',
		timeout: 2000,
		success: function(data) {
			list1 = data.categoryList[0] !== undefined ? data.categoryList[0].channelList : [];
			list2 = data.categoryList[1] !== undefined ? data.categoryList[1].channelList : [];
			list3 = data.categoryList[2] !== undefined ? data.categoryList[2].channelList : [];
			list4 = data.categoryList[3] !== undefined ? data.categoryList[3].channelList : [];
		},
		error: function() {
			console.error('【获取直播频道列表失败】');
		},
	})
}

/**
 * 打开直播播放页面
 */
function openPage(id, url, channelId, title, bindChannel, multicastUrl) {
	var a1 = document.getElementById(id);
	a1.addEventListener('click', function() {
		plus.plugintest.openLivePage(url, channelId, title, bindChannel, multicastUrl);
	})
}

// 退出APP
function quit() {
	var backButtonPress = 0;
	mui.back = function(event) {
		backButtonPress++;
		if(backButtonPress > 1) {
			plus.runtime.quit();
		} else {
			plus.nativeUI.toast('再按一次退出应用');
		}
		setTimeout(function() {
			backButtonPress = 0;
		}, 1000);
		return false;
	};
}