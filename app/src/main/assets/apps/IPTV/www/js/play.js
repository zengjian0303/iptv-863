// 根据具体的日期将星期设为今天
function getDays() {
	var days = ['周日', '周一', '周二', '周三', '周四', '周五', '周六'];
	var week = new Date().getDay();
	days[week] = '今天';
	var result = [];
	if(week !== 0) {
		result = result.concat(days.slice(week), days.slice(0, week));
	} else {
		result = days;
	}
	return result;
}

function getWeek() {
	var days = ['周日', '周一', '周二', '周三', '周四', '周五', '周六'];

	var str = "";
	var week = new Date().getDay();

	return days[week];
}

function setWeekDay() {
	var days = ['周日', '周一', '周二', '周三', '周四', '周五', '周六'];
	var weeks = document.getElementsByClassName('week-day');
	var week = new Date().getDay();
	week = week - 3;
	for(var i = 0; i < 7; i++) {
		if(i === 3) {
			weeks[3].innerHTML = '今天';
		} else {
			weeks[i].innerHTML = days[(week + 7) % 7];
		}
		week++;
	}
}

function genWhatDay() {
	var days = getDays();
	var whatDay = document.getElementById('whatDay');
	var html = '';
	for(var i = 0; i < days.length; i++) {
		if(days[i] === '今天') {
			html += '<a class="mui-control-item mui-active" href="#item"' + i + '>' + days[i] + '</a>'
			// document.getElementById('item' + i).className = "mui-slider-item mui-control-content mui-active"; 
		} else {
			html += '<a class="mui-control-item" href="#item"' + i + '>' + days[i] + '</a>'
		}
	}
	whatDay.innerHTML = html;
}

function genSliderGroup(index) {
	var html = '';

	for(var i = 0; i < 10; i++) {
		html += '<li class="mui-table-view-cell">第' + index + '个选项卡子项-' + i + '</li>';
	}

	document.getElementById('tableView' + index).innerHTML = html;

}