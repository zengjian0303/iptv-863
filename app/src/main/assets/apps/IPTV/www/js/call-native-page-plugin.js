document.addEventListener("plusready", function() {
	var _BARCODE = 'CallNativePagePlugin',
		B = window.plus.bridge;
	var plugintest = {
		openVodPage: function(Argus1, Argus2, Argus3, Argus4) {
			return B.execSync(_BARCODE, "openVodPage", [Argus1, Argus2, Argus3, Argus4]);
		},
		openLivePage: function(Argus1, Argus2, Argus3, Argus4, Argus5, Argus6) {
			return B.execSync(_BARCODE, "openLivePage", [Argus1, Argus2, Argus3, Argus4, Argus5, Argus6]);
		}
	};
	window.plus.plugintest = plugintest;
}, true);