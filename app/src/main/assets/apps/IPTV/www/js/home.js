mui.init({});

mui.plusReady(function() {

	initSlide();
	setRecommend();

	mui('.mui-slider').slider({
		interval: 5000 //自动轮播周期，若为0则不自动播放，默认为0；
	});

	//Tab切换,找到容器组件
	mui(".mui-bar-tab").on('tap', '.mui-tab-item', function(e) {
		$('.container-div').css({
			"display": "none"
		});
		$('.container-div').eq($(this).index()).css({
			"display": "block"
		});
		//		mui('.mui-scroll-wrapper').scroll().scrollTo(0, 0, 0); //100毫秒滚动到顶
	});

	//监听网络状态
	//document.addEventListener("netchange", onNetChange, false);

	openPage('a1');

	// 关闭应用
	quit();
})

/**
 * 打开点播播放页面
 */
function openPage(id, mediaId) {
	var a = document.getElementById(id);
	a.addEventListener('tap', function() {
		plus.plugintest.openVodPage(mediaId);

	})
}

/**
 * 首页推荐内容获取
 */
function setRecommend() {
	//var url = base_url + 'client/hot_recommand';
	//var url = BASE_URL + '/client/home/recommend';
	var user = JSON.parse(localStorage.getItem('$user') || '{}');
	var userId = '075500001269';
	mui.ajax(HOME_RECOMMEND, {
		data: {
			userId: user.account
		},
		dataType: 'json',
		type: 'POST',
		timeout: 10000,
		success: function(response) {
			var data = response.data;
			if(data !== null) {
				for(var i = 0; i < data.length; i++) {
					var recommend = document.getElementById('recommend' + i);
					var j = 0;

					mui.each(data[i].list, function(index, item) {
						var li = document.createElement('li');
						li.className = 'mui-table-view-cell mui-media mui-col-xs-4';
						var a_id = 'recommend' + i + '' + (j++);
						li.innerHTML = '<a id=' + a_id + '>\
										<div class= "bgDiv">\
											<img class="mui-media-object" src="' + base_img_url + item.img + '"/>\
											<div class="mui-media-body">\
												<p>' + item.title + '</p>\
											</div>\
										</div>\
									</a>';
						recommend.appendChild(li);
						openPage(a_id, item.mediaId);
					});
				}
			}
		},
		error: function(error) {
			console.error("【无法获取首页推荐数据】");
		}
	});
}

// 轮播图数据
function initSlide() {
	// 轮播图数据请求
	var url = base_url + 'client/slide/0';
	mui.ajax(url, {
		dataType: 'json',
		type: 'get',
		timeout: 10000,
		success: function(data) {
			var slides = data.slideList;
			var slideLoop = document.getElementById('slideLoop');
			var html = '<div class="mui-slider-item mui-slider-item-duplicate"><a href="#"><img src="' + base_img_url + slides[slides.length - 1].img + '"><p class="mui-slider-title"><font style="float: left;">' + slides[slides.length - 1].title + '</font><font style="float: right;">更新至第8集</font></p></img></a></div>';
			for(var i = 0; i < 4; i++) {
				html += '<div class="mui-slider-item"><a id="slide' + i + '" href="#"><img src="' + base_img_url + slides[i].img + '"/><p class="mui-slider-title">' + slides[i].title + '</p></a></div>';
			}
			html += '<div class="mui-slider-item mui-slider-item-duplicate"><a href="#"><img src="' + base_img_url + slides[0].img + '"><p class="mui-slider-title"><font style="float: left;">' + slides[0].title + '</font><font style="float: right;">更新至第8集</font></p></img></a></div>';
			slideLoop.innerHTML = html;

			for(var i = 0; i < 4; i++) {
				openPage('slide' + i, slides[i].mediaId);
			}

			mui('.mui-slider').slider({
				interval: 3000 // 自动轮播周期，若为0则不自动播放，默认为0；
			});
		},
		error: function(error) {
			console.log("无法获取轮播图数据!");
		}
	});
}

//用来处理列表数据的函数
function listDataAnalyze(data) {
	var arrayObj = data.T1348647853363;
	for(var i = 0; i < arrayObj.length; i++) {
		finalList = '<li class="mui-table-view-cell mui-media list-cell"><a href="#"><img class="mui-media-object mui-pull-left" src=' + arrayObj[i].img + '><div class="mui-media-body">' + arrayObj[i].title + '<p class="mui-ellipsis">' + arrayObj[i].digest + '</div></a></li>';
		$("#tableView-List").append(finalList);
	}
	$(document).on('tap', '.list-cell', function() {
		mui.openWindow({
			url: "detail.html",
			id: "detail",
			show: {
				autoShow: true, //页面loaded事件发生后自动显示，默认为true
				aniShow: "slide-in-right", //页面显示动画，默认为”slide-in-right“；
				duration: 200 //页面动画持续时间，Android平台默认100毫秒，iOS平台默认200毫秒；

			},
			extras: {
				listName: arrayObj[$(this).index()].id
			}
		})
	})
}

// 调用相机函数
function getCamera() {
	var cmr = plus.camera.getCamera();
	var path = null;
	cmr.captureImage(function(path) {
		//		//路径转换
		plus.io.resolveLocalFileSystemURL(path, function(entry) {
			//转换为本地路径
			var localUrl = entry.toLocalURL();
			//修改图片路径
			document.getElementById("camare-action").src = localUrl;
		});
	}, function(error) {
		alert("图片选择失败");
	}, {
		filename: "doc/camera/",
		index: 1
	});
}

//监听网络状态
function onNetChange() {
	var nt = plus.networkinfo.getCurrentType();
	switch(nt) {
		case plus.networkinfo.CONNECTION_ETHERNET:
		case plus.networkinfo.CONNECTION_WIFI:
			alert("已经连接wifi");
			break;
		case plus.networkinfo.CONNECTION_CELL2G:
		case plus.networkinfo.CONNECTION_CELL3G:
			alert("已经连接3G网络")
		case plus.networkinfo.CONNECTION_CELL4G:
			alert("已经连接蜂窝网络");
			break;
		default:
			alert("无网络连接");
			break;
	}
}

// 退出APP
function quit() {
	var backButtonPress = 0;
	mui.back = function(event) {
		backButtonPress++;
		if(backButtonPress > 1) {
			plus.runtime.quit();
		} else {
			plus.nativeUI.toast('再按一次退出应用');
		}
		setTimeout(function() {
			backButtonPress = 0;
		}, 1000);
		return false;
	};
}

// 调用原生Activity
function pluginShow() {
	plus.plugintest.PluginTestFunction("Html5", "Plus", "AsyncFunction", "MultiArgument!", function(result) {
		alert(result[0] + "_" + result[1] + "_" + result[2] + "_" + result[3]);
	}, function(result) {
		alert(result)
	});
}

function pluginShowArrayArgu() {
	plus.plugintest.PluginTestFunctionArrayArgu(["Html5", "Plus", "AsyncFunction", "ArrayArgument!"], function(result) {
		alert(result);
	}, function(result) {
		alert(result)
	});
}

function openLive(id) {
	plus.plugintest.PluginTestFunctionSync(id);
}

function pluginGetStringArrayArgu() {
	var Argus = plus.plugintest.PluginTestFunctionSyncArrayArgu(["Html5", "Plus", "SyncFunction", "ArrayArgument!"]);
	alert(Argus.RetArgu1 + "_" + Argus.RetArgu2 + "_" + Argus.RetArgu3 + "_" + Argus.RetArgu4);
}