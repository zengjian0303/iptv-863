package org.nood.iptv.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import org.nood.iptv.R;
import org.nood.iptv.entity.ProgramResult;
import org.nood.iptv.utils.DateUtils;

import java.util.List;

/**
 * VideoPager适配器
 *
 * @author Noodlewar
 * @date 2017/5/21
 */
public class ProgramListAdapter extends BaseAdapter {

    private Context context;
    private final List<ProgramResult> programLists;

    public ProgramListAdapter(Context context, List<ProgramResult> programLists) {
        this.context = context;
        this.programLists = programLists;
    }

    @Override
    public int getCount() {
        return programLists.size();
    }

    @Override
    public Fragment getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }


    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHoder viewHoder;
        if (view == null) {
            view = View.inflate(context, R.layout.program_pager_item, null);
            viewHoder = new ViewHoder();
            viewHoder.program_time = (TextView) view.findViewById(R.id.program_time);
            viewHoder.program_name = (TextView) view.findViewById(R.id.program_name);
            viewHoder.program_isPlay = (TextView) view.findViewById(R.id.program_isPlay);
            view.setTag(viewHoder);
        } else {
            viewHoder = (ViewHoder) view.getTag();
        }
        //根据position获取列表中对应位置的数据
        ProgramResult programResult = programLists.get(i);
        viewHoder.program_time.setText(DateUtils.trunDate(programResult.getStartTime()));
        viewHoder.program_name.setText(programResult.getTitle());
        viewHoder.program_isPlay.setText(statusDesc(programResult.getStatus()));
        return view;
    }

    private static class ViewHoder {
        TextView program_time;
        TextView program_name;
        TextView program_isPlay;
    }

    private static String statusDesc(int status) {
        String result;
        if (status == -1) {
            result = "已播完";
        } else if (status == 1) {
            result = "正在播出";
        } else {
            result = "未播出";
        }
        return result;
    }

}