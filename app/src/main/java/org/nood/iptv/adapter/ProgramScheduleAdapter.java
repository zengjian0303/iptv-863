package org.nood.iptv.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import org.nood.iptv.fragment.ProgramScheduleFragment;
import org.nood.iptv.utils.DateUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * 节目单Adapter
 *
 * @author Noodlewar
 * @date 2017/7/12
 */
public class ProgramScheduleAdapter extends FragmentPagerAdapter {

    private String[] titleArr;

    private String channelId;

    public ProgramScheduleAdapter(FragmentManager fm, String channelId) {
        super(fm);
        this.titleArr = DateUtils.initWeekArr();
        this.channelId = channelId;
    }

    @Override
    public Fragment getItem(int position) {
        return ProgramScheduleFragment.newInstance(channelId, position);
    }

    @Override
    public int getCount() {
        //默认为7天的节目表，这里因为数据获取只到今天，所以改为4，也就是今天到之前三天
        //return list.size();
        return 4;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return titleArr[position];
    }

}
