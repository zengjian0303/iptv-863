package org.nood.iptv.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import org.nood.iptv.R;
import org.nood.iptv.entity.ChannelResult;

import java.util.List;

/**
 * VideoPager适配器
 *
 * @author Noodlewar
 * @date 2017/5/21
 */
public class ChannelListAdapter extends BaseAdapter {

    private Context context;
    private final List<ChannelResult> channelResults;

    public ChannelListAdapter(Context context, List<ChannelResult> channelResults) {
        this.context = context;
        this.channelResults = channelResults;
    }

    @Override
    public int getCount() {
        return channelResults.size();
    }

    @Override
    public Fragment getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }


    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHoder viewHoder;
        if (view == null) {
            view = View.inflate(context, R.layout.program_pager_item, null);
            viewHoder = new ViewHoder();
            viewHoder.channel_time = (TextView) view.findViewById(R.id.program_time);
            viewHoder.channel_name = (TextView) view.findViewById(R.id.program_name);
            viewHoder.channel_isPlay = (TextView) view.findViewById(R.id.program_isPlay);
            view.setTag(viewHoder);
        } else {
            viewHoder = (ViewHoder) view.getTag();
        }
        //根据position获取列表中对应位置的数据
        ChannelResult channelResult = channelResults.get(i);
        viewHoder.channel_time.setText(channelResult.getTitle());
        viewHoder.channel_name.setText(channelResult.getTitle());
        viewHoder.channel_isPlay.setText(channelResult.getChannelId());
        return view;
    }

    private static class ViewHoder {
        TextView channel_time;
        TextView channel_name;
        TextView channel_isPlay;
    }

}