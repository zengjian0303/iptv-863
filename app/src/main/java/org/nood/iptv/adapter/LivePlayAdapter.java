package org.nood.iptv.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import org.nood.iptv.fragment.PageFragment;

import java.util.List;

/**
 *
 * @author Noodlewar
 * @date 2017/7/12
 */
public class LivePlayAdapter extends FragmentPagerAdapter {

    private String[] titleBar = {"节目单", "选台"};

    private String channelId;

    public LivePlayAdapter(FragmentManager fm, String channelId) {
        super(fm);
        this.channelId = channelId;
    }

    @Override
    public Fragment getItem(int position) {
        if (position == 0) {
            return PageFragment.newInstance(7, channelId);
        } else {
            return PageFragment.newInstance(4);
        }
    }

    @Override
    public int getCount() {
        return titleBar.length;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return titleBar[position];
    }

}
