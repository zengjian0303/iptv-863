package org.nood.iptv.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import org.nood.iptv.fragment.ChannelFragment;

import java.util.ArrayList;
import java.util.List;

/**
 * 频道适配器
 *
 * @author Noodlewar
 * @date 2017/7/12
 */
public class ChannelAdapter extends FragmentPagerAdapter {

    //private String[] titleArr = {"央视", "卫视", "地方", "其他"};
    private String[] titleArr = {"央视", "卫视", "地方"};

    public ChannelAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        return ChannelFragment.newInstance(position);
    }

    @Override
    public int getCount() {
        return titleArr.length;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return titleArr[position];
    }

}
