package org.nood.iptv.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import org.nood.iptv.R;
import org.nood.iptv.entity.RecommendItem;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.fresco.helper.Phoenix;

import java.util.List;

/**
 * 推荐列表适配器
 *
 * @author Noodlewar
 * @date 2017/8/3
 */
public class RecommendListAdapter extends BaseAdapter {

    private Context context;
    private final List<RecommendItem> recommendItems;

    public RecommendListAdapter(Context context, List<RecommendItem> recommendItems) {
        this.context = context;
        this.recommendItems = recommendItems;
    }

    @Override
    public int getCount() {
        return recommendItems.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        Phoenix.init(context);
        final ViewHolder viewHolder;
        if (view == null) {
            view = View.inflate(context, R.layout.recommend_item, null);
            viewHolder = new ViewHolder();
            viewHolder.poster = (SimpleDraweeView) view.findViewById(R.id.poster);
            viewHolder.video_name = (TextView) view.findViewById(R.id.video_name);
            viewHolder.video_content = (TextView) view.findViewById(R.id.video_content);
            view.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) view.getTag();
        }
        //根据position获取列表中对应位置的数据
        RecommendItem recommendItem = recommendItems.get(i);
        viewHolder.video_name.setText(recommendItem.getName());
        viewHolder.video_content.setText(recommendItem.getContent());

        Phoenix.with(viewHolder.poster).load(recommendItem.getPosterUrl());
        return view;
    }

    private static class ViewHolder {
        SimpleDraweeView poster;
        TextView video_name;
        TextView video_content;
    }

}