package org.nood.iptv.iptv;

/**
 * @author lcssx
 * @date 11/9/2017
 */
public class EpgTools {

    public static final String VOD_URL = "http://125.88.98.36:58082/epg/hdvod/biz_46876029/det/vod/";
    public static final String SERIES_URL = "http://125.88.98.36:58082/epg/hdvod/biz_46876029/det/series/";
    public static final String SUFFIX_URL = ".epg?user_id=${USERID}&stb_type=${STBTYPE}&sign=${USERTOKEN}&return=${BACKURL}&domain=${DOMAIN}";

    /**
     * 字符串转换成十六进制字符串
     */
    public static String str2HexStr(String str) {
        char[] chars = "0123456789ABCDEF".toCharArray();
        StringBuilder sb = new StringBuilder("");
        byte[] bs = str.getBytes();
        int bit;
        for (int i = 0; i < bs.length; i++) {
            bit = (bs[i] & 0x0f0) >> 4;
            sb.append(chars[bit]);
            bit = bs[i] & 0x0f;
            sb.append(chars[bit]);
        }
        return sb.toString();
    }

    /**
     * 十六进制转换字符串
     */
    public static String hexStr2Str(String hexStr) {
        String str = "0123456789ABCDEF";
        char[] hexs = hexStr.toCharArray();
        byte[] bytes = new byte[hexStr.length() / 2];
        int n;
        for (int i = 0; i < bytes.length; i++) {
            n = str.indexOf(hexs[2 * i]) * 16;
            n += str.indexOf(hexs[2 * i + 1]);
            bytes[i] = (byte) (n & 0xff);
        }
        return new String(bytes);
    }

    public static String getSeriesUrl(String bindCode) {
        return str2HexStr(SERIES_URL + bindCode + SUFFIX_URL);
    }

    public static String getVodUrl(String bindCode) {
        return str2HexStr(VOD_URL + bindCode + SUFFIX_URL);
    }

}
