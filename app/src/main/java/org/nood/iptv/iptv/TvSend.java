package org.nood.iptv.iptv;

import org.nood.iptv.utils.MD5Utils;

import java.net.URLEncoder;
import java.util.*;

/**
 * @author Noodlewar
 * @date 2017/11/28
 */
public class TvSend {

    private static final String PROJECT_SEVER_URL = "http://14.29.2.24:8080";
    // private static final String PROJECT_SEVER_URL = "http://msg.iptv.gd.cn:8080";
    private static final String EPG_JUMPURL = "http://125.88.102.153:8082/EPG/jsp/defaultsmchd/en/externalLinkJumpAgent.jsp?destUrl=";
    private static final String TVSERVER_URL = "http://14.29.2.24:8080";
    private static final String TVSERVER_URL_PARAMS = "&sid=201&appid=100";

    /**
     * 构造直播投屏URL
     *
     * @return
     */
    public static String buildLiveProjectScreenUrl(String channelIndex) {
        //String tmp = "sid=202&appid=100";
        String tmp = "sid=201&appid=100";
        String url = PROJECT_SEVER_URL + "/push/mgr.do?cmd=playChannel&channelIndex=" + channelIndex + "&receiver=08882017102702&" + tmp;

        long now = System.currentTimeMillis();
        url += "&timestamp=" + now;

        url += "&sig=" + getSig(url, null);
        return url;
    }

    /**
     * 用hls播放地址构造直播投屏URL
     *
     * @param playUrl 播放地址
     * @return 拼接后的播放地址
     */
    public static String buildLiveProjectScreenUrlByHls(String playUrl) {

        // 拼接播放地址
        playUrl = "/EPG/jsp/defaultsmchd/en/play/dj_playTrailer.jsp?playUrl=" + playUrl;

        String pageJumpUrl = EPG_JUMPURL + EpgTools.str2HexStr(playUrl);

        String sendUrl = TVSERVER_URL + "/push/mgr.do?cmd=playTest&url=" + URLEncoder.encode(pageJumpUrl) + "&receiver=" + "08882017102702" + TVSERVER_URL_PARAMS;

        long now = System.currentTimeMillis();
        sendUrl += "&timestamp=" + now;

        String sigUrl = sendUrl + "&sig=" + getSig(sendUrl, null);

        return sigUrl;
    }

    /**
     * 构造点播投屏URL
     *
     * @return
     */
    public static String buildVodProjectScreenUrl(String iptvCode) {
        StringBuilder builder = new StringBuilder();
        builder.append(PROJECT_SEVER_URL);
        builder.append("/push/mgr.do?cmd=playVod&foreignId=");
        builder.append(iptvCode);
        builder.append("&receiver=08882017102702&");
        builder.append("sid=202&appid=100");
        builder.append("&timestamp=");
        builder.append(System.currentTimeMillis());

        String sig = getSig(builder.toString(), null);

        builder.append("&sig=");
        builder.append(sig);
        return builder.toString();
    }

    public static String getSig(String url, String body) {
        Map<String, String> params = new HashMap<>();
        url = url.substring(url.indexOf("?") + 1);
        for (String term : url.split("&")) {
            String[] v = term.split("=");
            String key = v[0];
            String value = v.length > 1 ? v[1] : "";
            params.put(key, value);
        }
        if (body != null) {
            params.put("content", body);
        }

        List<String> keys = new ArrayList<>();
        keys.addAll(params.keySet());
        Collections.sort(keys);
        String md5key = "test123";
        StringBuilder sb = new StringBuilder(md5key);
        for (String k : keys) {
            if ("sig".equals(k)) {
                continue;
            }
            sb.append("#").append(params.get(k));
        }

        try {
            return MD5Utils.md5(sb.toString(), "utf-8");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

}
