package org.nood.iptv.entity;

/**
 * Created by Noodlewar on 2017/8/3.
 */
public class RecommendItem {

    private String posterUrl;
    private String name;
    private String content;
    private String iptvCode;

    public String getPosterUrl() {
        return posterUrl;
    }

    public void setPosterUrl(String posterUrl) {
        this.posterUrl = posterUrl;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getIptvCode() {
        return iptvCode;
    }

    public void setIptvCode(String iptvCode) {
        this.iptvCode = iptvCode;
    }

    @Override
    public String toString() {
        return "RecommendItem{" +
                "posterUrl='" + posterUrl + '\'' +
                ", name='" + name + '\'' +
                ", content='" + content + '\'' +
                ", iptvCode='" + iptvCode + '\'' +
                '}';
    }

}
