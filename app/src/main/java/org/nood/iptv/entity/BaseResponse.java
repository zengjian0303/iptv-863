package org.nood.iptv.entity;

import java.io.Serializable;

/**
 * Created by Noodlewar on 2017/8/1.
 */
public class BaseResponse<T> implements Serializable {

    public int result;
    public T data;

    @Override
    public String toString() {
        return "BaseResponse{" +
                "result=" + result +
                ", data=" + data +
                '}';
    }

}
