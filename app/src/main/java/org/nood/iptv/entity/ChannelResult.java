package org.nood.iptv.entity;

/**
 * 频道
 * Created by Noodlewar on 2017/8/1.
 */
public class ChannelResult {

    private String channelId;
    private String title;
    private String playURL;
    private String description;

    public String getChannelId() {
        return channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPlayURL() {
        return playURL;
    }

    public void setPlayURL(String playURL) {
        this.playURL = playURL;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "ChannelResult{" +
                "channelId='" + channelId + '\'' +
                ", title='" + title + '\'' +
                ", playURL='" + playURL + '\'' +
                ", description='" + description + '\'' +
                '}';
    }

}
