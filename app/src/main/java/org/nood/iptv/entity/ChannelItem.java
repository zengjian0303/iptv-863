package org.nood.iptv.entity;

/**
 * 电视频道
 * Created by Noodlewar on 2017/7/18.
 */
public class ChannelItem {

    /**
     * 节目名
     */
    private String channelName;
    /**
     * 节目播出时间
     */
    private String channelTime;
    /**
     * 是否正在播出
     */
    private String channelIsPlay;

    public ChannelItem() {
    }

    public ChannelItem(String channelName, String channelTime, String channelIsPlay) {
        this.channelName = channelName;
        this.channelTime = channelTime;
        this.channelIsPlay = channelIsPlay;
    }

    public String getChannelName() {
        return channelName;
    }

    public void setChannelName(String channelName) {
        this.channelName = channelName;
    }

    public String getChannelTime() {
        return channelTime;
    }

    public void setChannelTime(String channelTime) {
        this.channelTime = channelTime;
    }

    public String getChannelIsPlay() {
        return channelIsPlay;
    }

    public void setChannelIsPlay(String channelIsPlay) {
        this.channelIsPlay = channelIsPlay;
    }

}
