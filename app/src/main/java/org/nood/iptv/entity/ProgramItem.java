package org.nood.iptv.entity;

/**
 * 节目
 * Created by Noodlewar on 2017/7/18.
 */
public class ProgramItem {

    /**
     * 节目名
     */
    private String programName;
    /**
     * 节目播出时间
     */
    private String programTime;
    /**
     * 是否正在播出
     */
    private String programIsPlay;

    public ProgramItem() {
    }

    public ProgramItem(String programName, String programTime, String programIsPlay) {
        this.programName = programName;
        this.programTime = programTime;
        this.programIsPlay = programIsPlay;
    }

    public String getProgramName() {
        return programName;
    }

    public void setProgramName(String programName) {
        this.programName = programName;
    }

    public String getProgramTime() {
        return programTime;
    }

    public void setProgramTime(String programTime) {
        this.programTime = programTime;
    }

    public String getProgramIsPlay() {
        return programIsPlay;
    }

    public void setProgramIsPlay(String programIsPlay) {
        this.programIsPlay = programIsPlay;
    }

}
