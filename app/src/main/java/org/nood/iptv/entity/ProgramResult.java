package org.nood.iptv.entity;

/**
 * Created by Noodlewar on 2017/8/1.
 */
public class ProgramResult {

    private String startTime;
    private String runTime;
    private String title;
    private String scheduleId;
    private int status;
    private String playseekURL;

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getRunTime() {
        return runTime;
    }

    public void setRunTime(String runTime) {
        this.runTime = runTime;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getScheduleId() {
        return scheduleId;
    }

    public void setScheduleId(String scheduleId) {
        this.scheduleId = scheduleId;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getPlayseekURL() {
        return playseekURL;
    }

    public void setPlayseekURL(String playseekURL) {
        this.playseekURL = playseekURL;
    }

    @Override
    public String toString() {
        return "ProgramResult{" +
                "startTime='" + startTime + '\'' +
                ", runTime='" + runTime + '\'' +
                ", title='" + title + '\'' +
                ", scheduleId='" + scheduleId + '\'' +
                ", status=" + status +
                ", playseekURL='" + playseekURL + '\'' +
                '}';
    }

}
