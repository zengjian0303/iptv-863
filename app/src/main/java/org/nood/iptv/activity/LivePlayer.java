package org.nood.iptv.activity;

import android.annotation.SuppressLint;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.view.GestureDetector;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.source.hls.HlsMediaSource;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.ui.SimpleExoPlayerView;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;
import com.lzy.okgo.OkGo;
import com.lzy.okgo.callback.StringCallback;
import com.lzy.okgo.model.Response;

import org.nood.iptv.R;
import org.nood.iptv.adapter.LivePlayAdapter;
import org.nood.iptv.entity.MediaItem;
import org.nood.iptv.fragment.ChannelFragment;
import org.nood.iptv.fragment.ProgramScheduleFragment;
import org.nood.iptv.iptv.TvSend;
import org.nood.iptv.utils.DensityUtils;
import org.nood.iptv.utils.LogUtils;
import org.nood.iptv.utils.Utils;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * 直播播放器
 *
 * @author Noodlewar
 * @date 2017/5/21
 */
public class LivePlayer extends AppCompatActivity implements View.OnClickListener, ProgramScheduleFragment.OnScheduleSelectedListener, ChannelFragment.OnChannelSelectedListener {

    /**
     * 视频进度的更新
     */
    private static final int PROGRESS = 1;
    /**
     * 隐藏控制面板
     */
    private static final int HIDE_MEDIA_CONTROLLER = 2;
    /**
     * 显示当前网速
     */
    private static final int SHOW_NET_SPEED = 3;
    /**
     * 全屏
     */
    private static final int FULL_SCREEN = 1;
    /**
     * 默认
     */
    private static final int DEFAULT_SCREEN = 2;
    @BindView(R.id.simple_exo_player_view)
    SimpleExoPlayerView simpleExoPlayerView;
    private Uri uri;
    /**
     * 视频播放页面布局
     */
    @BindView(R.id.ll_video)
    LinearLayout llVideo;
    @BindView(R.id.ll_media_msg)
    LinearLayout llMediaMsg;
    @BindView(R.id.btn_media_msg)
    Button btnMediaMsg;
    @BindView(R.id.sb_voice)
    SeekBar sbVoice;
    @BindView(R.id.btn_projector)
    Button btnProjector;
    @BindView(R.id.media_control)
    RelativeLayout mediaControl;
    @BindView(R.id.ll_player_bottom_seekbar)
    LinearLayout ll_player_bottom_seekbar;
    @BindView(R.id.tv_current_time)
    TextView tvCurrentTime;
    @BindView(R.id.sb_video)
    SeekBar sbVideo;
    @BindView(R.id.tv_duration)
    TextView tvDuration;
    @BindView(R.id.btn_exit)
    Button btnExit;
    @BindView(R.id.btn_video_pre)
    Button btnVideoPre;
    @BindView(R.id.btn_video_start_pause)
    Button btnVideoStartPause;
    @BindView(R.id.btn_video_next)
    Button btnVideoNext;
    @BindView(R.id.btn_video_switch_screen)
    Button btnVideoSwitchScreen;
    @BindView(R.id.ll_loading)
    LinearLayout llLoading;
    /**
     * 节目表、频道Tab
     */
    @BindView(R.id.main_tab_layout)
    TabLayout main_tab_layout;
    /**
     * 标题栏:测试
     */
    @BindView(R.id.main_toolbar)
    Toolbar mainToolBar;
    @BindView(R.id.tv_toolbar_title)
    TextView tvToolBarTitle;
    @BindView(R.id.main_vp)
    ViewPager main_vp;
    /**
     * 播放布局
     */
    @BindView(R.id.play_pager)
    RelativeLayout rl;
    /**
     * 退出当前页
     */
    @BindView(R.id.back_btn)
    ImageView back_btn;
    /**
     * 传入的视频列表
     */
    private List<MediaItem> mediaItems;
    //要播放的列表中的具体位置
    private int position;
    //定义手势识别器
    private GestureDetector detector;
    /**
     * 是否显示控制面板
     */
    private boolean isShowMediaController = false;
    /**
     * 是否为全屏
     */
    private boolean isFullScreen = false;
    //屏幕的宽
    private int screenWidth = 0;
    //屏幕的高
    private int screenHeight = 0;

    /**
     * 视频真实的宽和高
     */
    private int videoWidth = 0;
    private int videoHeight = 0;

    /**
     * 调节声音
     */
    private AudioManager audioManager;
    /**
     * 当前音量
     */
    private int currentVolume;
    /**
     * 等级0-15
     * 最大音量
     */
    private int maxVolume;
    private boolean isMute = false;

    private SimpleExoPlayer player;
    MediaSource videoSource;
    private DefaultDataSourceFactory dataSourceFactory;
    private DefaultExtractorsFactory extractorsFactory;
    private Handler mainHandler;
    /**
     * 直播频道ID
     */
    private String channelId;
    private String title;
    /**
     * 直播播放地址
     */
    private String playURL;
    /**
     * 组播播放地址
     */
    private String multicastUrl;
    private String bindChannel;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initData();

        getData();

        findViews();

        setListener();

        setData();

        DensityUtils.init(this);
    }

    /**
     * 绑定事件监听
     */
    private void findViews() {
        setContentView(R.layout.activity_live_player);
        ButterKnife.bind(this);

        btnProjector.setOnClickListener(this);
        btnExit.setOnClickListener(this);
        btnVideoPre.setOnClickListener(this);
        btnVideoStartPause.setOnClickListener(this);
        btnVideoNext.setOnClickListener(this);
        btnVideoSwitchScreen.setOnClickListener(this);
        back_btn.setOnClickListener(this);

        ll_player_bottom_seekbar.setVisibility(View.INVISIBLE);

        //设置ToolBar标题
        tvToolBarTitle.setText(title);
        //最大音量和SeekBar关联
        sbVoice.setMax(maxVolume);
        //设置当前音量
        sbVoice.setProgress(currentVolume);

        //开始更新网速
        handler.sendEmptyMessage(SHOW_NET_SPEED);

        mainHandler = new Handler();
        DefaultBandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
        TrackSelection.Factory videoTrackSelectionFactory =
                new AdaptiveTrackSelection.Factory(bandwidthMeter);
        TrackSelector trackSelector =
                new DefaultTrackSelector(videoTrackSelectionFactory);

        player = ExoPlayerFactory.newSimpleInstance(this, trackSelector);


        simpleExoPlayerView.setPlayer(player);
        simpleExoPlayerView.setUseController(false);

        dataSourceFactory = new DefaultDataSourceFactory(this,
                Util.getUserAgent(this, "yourApplicationName"), bandwidthMeter);

        extractorsFactory = new DefaultExtractorsFactory();
        if (playURL.endsWith(".m3u8")) {
            videoSource = new HlsMediaSource(Uri.parse(playURL), dataSourceFactory, mainHandler, null);
        } else {
            videoSource = new ExtractorMediaSource(Uri.parse(playURL),
                    dataSourceFactory, extractorsFactory, null, null);
        }

        player.prepare(videoSource);
        player.setPlayWhenReady(true);
        player.addListener(new ExoPlayer.EventListener() {
            @Override
            public void onTimelineChanged(Timeline timeline, Object manifest) {

            }

            @Override
            public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {

            }

            @Override
            public void onLoadingChanged(boolean isLoading) {
                if (isLoading) {
                    llLoading.setVisibility(View.GONE);
                }
            }

            @Override
            public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {

            }

            @Override
            public void onPlayerError(ExoPlaybackException error) {
                Toast.makeText(LivePlayer.this, "无法播放该视频！", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onPositionDiscontinuity() {

            }

            @Override
            public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {

            }
        });

        //String channelId = "9160";
        main_vp.setAdapter(new LivePlayAdapter(getSupportFragmentManager(), channelId));

        main_tab_layout.setupWithViewPager(main_vp);
    }

    @Override
    public void onClick(View v) {
        if (v == btnProjector) {
            projectLive();
        } else if (v == btnExit) {
            finish();
        } else if (v == btnVideoPre) {
            playPreVideo();
        } else if (v == btnVideoStartPause) {
            startAndPause();
        } else if (v == btnVideoNext) {
            playNextVideo();
        } else if (v == btnVideoSwitchScreen) {
            switchScreen();
        } else if (v == back_btn) {
            finish();
        }

        handler.removeMessages(HIDE_MEDIA_CONTROLLER);
        handler.sendEmptyMessageDelayed(HIDE_MEDIA_CONTROLLER, 3000);
    }

    private void startAndPause() {
        if (player.getPlayWhenReady()) {
            //视频在播放,设置为暂停,按钮设置为播放
            player.setPlayWhenReady(false);
            btnVideoStartPause.setBackgroundResource(R.drawable.btn_video_start_selector);
        } else {
            //视频暂停,设置为播放,按钮设置为暂停
            player.setPlayWhenReady(true);
            btnVideoStartPause.setBackgroundResource(R.drawable.btn_video_pause_selector);
        }
    }

    /**
     * 播放上一个视频
     */
    private void playPreVideo() {
        if (mediaItems != null && mediaItems.size() > 0) {
            //播放上一个视频
            position--;
            if (position >= 0) {

                //ll_loading.setVisibility(View.VISIBLE);

                MediaItem mediaItem = mediaItems.get(position);
                //vv_video.setVideoPath(mediaItem.getData());

                //设置按钮状态
                setButtonStatus();
            }
        } else if (uri != null) {
            //设置按钮状态--上一个和下一个按钮设置为灰色并且不可以点击
            setButtonStatus();
        }
    }

    /**
     * 播放下一个视频
     */
    private void playNextVideo() {
        if (mediaItems != null && mediaItems.size() > 0) {
            //播放下一个
            position++;
            if (position < mediaItems.size()) {

                //ll_loading.setVisibility(View.VISIBLE);

                MediaItem mediaItem = mediaItems.get(position);
                //vv_video.setVideoPath(mediaItem.getData());

                //设置按钮状态
                setButtonStatus();
            }
        } else if (uri != null) {
            //上一个和下一个按钮设置为灰色并且不可以点击
            setButtonStatus();
        }
    }

    private void setButtonStatus() {
        if (mediaItems != null && mediaItems.size() > 0) {
            if (mediaItems.size() == 1) {
                //两个按钮都设置为灰色
                setEnable(false);
            } else if (mediaItems.size() == 2) {
                if (position == 0) {
                    btnVideoPre.setBackgroundResource(R.drawable.btn_pre_gray);
                    btnVideoPre.setEnabled(false);

                    btnVideoNext.setBackgroundResource(R.drawable.btn_video_next_selector);
                    btnVideoNext.setEnabled(false);
                } else if (position == mediaItems.size() - 1) {
                    btnVideoNext.setBackgroundResource(R.drawable.btn_next_gray);
                    btnVideoNext.setEnabled(false);

                    btnVideoPre.setBackgroundResource(R.drawable.btn_video_pre_selector);
                    btnVideoPre.setEnabled(true);
                } else {
                    setEnable(true);
                }
            } else {
                if (position == 0) {
                    btnVideoPre.setBackgroundResource(R.drawable.btn_pre_gray);
                    btnVideoPre.setEnabled(false);
                } else if (position == mediaItems.size() - 1) {
                    btnVideoNext.setBackgroundResource(R.drawable.btn_next_gray);
                    btnVideoNext.setEnabled(false);
                } else {
                    setEnable(true);
                }
            }
        } else if (uri != null) {
            //两个按钮都设置为灰色
            btnVideoPre.setBackgroundResource(R.drawable.btn_pre_gray);
            btnVideoPre.setEnabled(false);
            btnVideoNext.setBackgroundResource(R.drawable.btn_next_gray);
            btnVideoNext.setEnabled(false);
        }
    }

    private void setEnable(boolean flag) {
        if (flag) {
            btnVideoPre.setBackgroundResource(R.drawable.btn_video_pre_selector);
            btnVideoPre.setEnabled(true);
            btnVideoNext.setBackgroundResource(R.drawable.btn_video_next_selector);
            btnVideoNext.setEnabled(true);
        } else {
            btnVideoPre.setBackgroundResource(R.drawable.btn_pre_gray);
            btnVideoPre.setEnabled(false);
            btnVideoNext.setBackgroundResource(R.drawable.btn_next_gray);
            btnVideoNext.setEnabled(false);
        }
    }

    private void setData() {
        if (mediaItems != null && mediaItems.size() > 0) {
            MediaItem mediaItem = mediaItems.get(position);
            //设置视频的名称
            //vv_video.setVideoPath(mediaItem.getData());
        } else if (uri != null) {
            //设置视频的名称
        } else {
            Toast.makeText(LivePlayer.this, "没有传递数据", Toast.LENGTH_SHORT).show();
        }
        setButtonStatus();
    }

    private void getData() {
        //获取播放地址
        // uri = getIntent().getData();// 文件夹，图片浏览器
        //uri = Uri.parse("http://vf2.mtime.cn/Video/2017/05/06/mp4/170506082411224771.mp4");
        //uri = Uri.parse("http://125.88.92.230:30001/PLTV/88888956/224/3221227674/index.m3u8");

        multicastUrl = getIntent().getStringExtra("multicastUrl");
        playURL = getIntent().getStringExtra("playURL");
        channelId = getIntent().getStringExtra("channelId");
        title = getIntent().getStringExtra("title");
        bindChannel = getIntent().getStringExtra("bindChannel");
        //uri = Uri.parse("http://125.88.92.230:30001/PLTV/88888956/224/3221227666/index.m3u8");
        //playURL = "http://172.19.120.61:8080/ArcSoft_4k_1/ArcSoft_4k_1.m3u8";
        //playURL = "http://vf2.mtime.cn/Video/2017/05/06/mp4/170506082411224771.mp4";
        uri = Uri.parse(playURL);

        // 华为测试直播地址
        // uri = Uri.parse("http://192.168.204.208/PLTV/88888888/224/3221225484/index.m3u8?servicetype=1");

        mediaItems = (List<MediaItem>) getIntent().getSerializableExtra("videolist");
        position = getIntent().getIntExtra("position", 0);
    }

    private void initData() {
        //实例化手势识别器，并重写双击，单击，长按
        detector = new GestureDetector(this, new GestureDetector.SimpleOnGestureListener() {
            @Override
            public void onLongPress(MotionEvent e) {
                super.onLongPress(e);
                startAndPause();
            }

            @Override
            public boolean onDoubleTap(MotionEvent e) {
                switchScreen();
                return super.onDoubleTap(e);
            }

            @Override
            public boolean onSingleTapConfirmed(MotionEvent e) {
                if (isShowMediaController) {
                    //隐藏
                    hideMediaController();
                    //把隐藏消息移除
                    handler.removeMessages(HIDE_MEDIA_CONTROLLER);
                } else {
                    //显示
                    showMediaController();
                    //发消息隐藏
                    handler.sendEmptyMessageDelayed(HIDE_MEDIA_CONTROLLER, 3000);
                }
                return super.onSingleTapConfirmed(e);
            }
        });

        //获取音量
        audioManager = (AudioManager) getSystemService(AUDIO_SERVICE);
        currentVolume = audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
        maxVolume = audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
    }

    private void switchScreen() {
        /*if (isFullScreen) {
            //默认
            setVideoType(DEFAULT_SCREEN);
        } else {
            //全屏
            setVideoType(FULL_SCREEN);
        }*/
        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        } else if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
    }

    private void setVideoType(int screenType) {
        //获取屏幕的宽和高(新)
        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        screenWidth = metrics.widthPixels;
        screenHeight = metrics.heightPixels;
        switch (screenType) {
            case FULL_SCREEN: //全屏
                //设置视频画面的大小
                //vv_video.setVideoSize(screenWidth, screenHeight);
                //设置按钮的状态-默认
                btnVideoSwitchScreen.setBackgroundResource(R.drawable.btn_video_switch_default_screen_selector);
                isFullScreen = true;
                break;
            case DEFAULT_SCREEN: //默认
                //设置视频画面的大小

                //视频真实的宽和高
                int mVideoWidth = videoWidth;
                int mVideoHeight = videoHeight;

                int width = screenWidth;
                int height = screenHeight;
                if (mVideoWidth * height < width * mVideoHeight) {
                    width = height * mVideoWidth / mVideoHeight;
                } else if (mVideoWidth * height > width * mVideoHeight) {
                    height = width * mVideoHeight / mVideoWidth;
                }
                //vv_video.setVideoSize(width, height);
                //设置按钮的状态-全屏
                btnVideoSwitchScreen.setBackgroundResource(R.drawable.btn_video_switch_default_screen_selector);
                isFullScreen = false;
                break;
            default:
                break;
        }
    }

    /**
     * 直播播放页面节目单回看功能
     *
     * @param url
     */
    @Override
    public void onScheduleSelected(String url) {
        if (url != null && !"".equals(url)) {
            if (url.contains(".m3u8")) {
                videoSource = new HlsMediaSource(Uri.parse(url), dataSourceFactory, mainHandler, null);
            } else {
                videoSource = new ExtractorMediaSource(Uri.parse(url),
                        dataSourceFactory, extractorsFactory, null, null);
            }
            player.prepare(videoSource);
            player.setPlayWhenReady(true);
        }
    }

    @Override
    public void onChannelSelected(String url) {
        if (url.contains(".m3u8")) {
            videoSource = new HlsMediaSource(Uri.parse(url), dataSourceFactory, mainHandler, null);
        } else {
            videoSource = new ExtractorMediaSource(Uri.parse(url),
                    dataSourceFactory, extractorsFactory, null, null);
        }

        player.prepare(videoSource);
        player.setPlayWhenReady(true);
    }

    private void setListener() {
        //准备好的监听
        //vv_video.setOnPreparedListener(new MyOnPreparedListener());

        //播放出错的监听
        //vv_video.setOnErrorListener(new MyOnErrorListener());

        //播放完成的监听
        //vv_video.setOnCompletionListener(new MyOnCompletionListener());

        //设置SeekBar状态变化的监听
        sbVideo.setOnSeekBarChangeListener(new VideoOnSeekBarChangeListener());

        sbVoice.setOnSeekBarChangeListener(new VolumeOnSeekBarChangeListener());

    }

    private class VolumeOnSeekBarChangeListener implements SeekBar.OnSeekBarChangeListener {

        @Override
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            if (fromUser) {
                isMute = progress <= 0;
                updateVolume(progress, isMute);
            }
        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {

        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {

        }

    }

    /**
     * 设置音量大小
     *
     * @param progress 音量进度
     */
    private void updateVolume(int progress, boolean isMute) {
        if (isMute) {
            audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, 0, 0);
            sbVoice.setProgress(0);
        } else {
            audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, progress, 0);
            sbVoice.setProgress(progress);
            currentVolume = progress;
        }
    }


    private class VideoOnSeekBarChangeListener implements SeekBar.OnSeekBarChangeListener {
        /**
         * 当手指滑动的时候，会引起SeekBar进度变化，会回调该方法
         *
         * @param progress 当前进度
         * @param fromUser 进度条变化如果是用户引起的true，不是则为false
         */
        @Override
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            if (fromUser) {
                player.seekTo(progress);
            }
        }

        /**
         * 当手指触碰的时候回调该方法
         */
        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {
            handler.removeMessages(HIDE_MEDIA_CONTROLLER);
        }

        /**
         * 当手指离开的时候回调该方法
         */
        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {
            handler.sendEmptyMessageDelayed(HIDE_MEDIA_CONTROLLER, 3000);
        }
    }

    @SuppressLint("HandlerLeak")
    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case SHOW_NET_SPEED:
                    //获取网速
                    String netSpeed = Utils.getNetSpeed(LivePlayer.this);

                    //显示网络速度
                    //tv_loading_net_speed.setText("玩命加载中..." + netSpeed);
                    //tv_net_speed.setText("缓冲中..." + netSpeed);

                    //每两秒更新一次
                    handler.removeMessages(SHOW_NET_SPEED);
                    handler.sendEmptyMessageDelayed(SHOW_NET_SPEED, 2000);

                    break;
                case HIDE_MEDIA_CONTROLLER: //隐藏控制面板
                    hideMediaController();
                    break;
                case PROGRESS:
                    //得到当前视频的播放进度
                    int currentPosition = (int) player.getCurrentPosition();
                    //SeekBar.setProgress(当前进度)
                    sbVideo.setProgress(currentPosition);

                    //更新文本播放进度
                    tvCurrentTime.setText(Utils.stringForTime(currentPosition));

                    //缓冲进度的更新
//                    if (isNetUri) {
//                        //只有网络资源才有缓冲
//                        int buffer = vv_video.getBufferPercentage();
//                        int totalBuffer = sbVideo.getMax();
//                        int secondaryProgress = totalBuffer / 100;
//                        sbVideo.setSecondaryProgress(secondaryProgress);
//                    } else {
//                        //本地视频没有缓冲
//                        sbVideo.setSecondaryProgress(0);
//                    }
                    sbVideo.setSecondaryProgress(0);

                    //每秒更新一次
                    handler.removeMessages(PROGRESS);
                    handler.sendEmptyMessageDelayed(PROGRESS, 1000);
                    break;
                default:
                    break;
            }
        }
    };

    @Override
    protected void onDestroy() {
        //移除所有的消息
        handler.removeCallbacksAndMessages(null);

        LogUtils.e("LivePlayActivity is destroyed");
        player.release();
        super.onDestroy();
    }

    private float startY;
    /**
     * 屏幕的高
     */
    private float touchRang;
    /**
     * 当一按下的音量
     */
    private int mVol;

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        //把事件传递给手势识别器
        detector.onTouchEvent(event);

        switch (event.getAction()) {
            // 手指按下
            case MotionEvent.ACTION_DOWN:
                // 按下时记录值
                startY = event.getY();
                mVol = audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
                touchRang = Math.min(screenHeight, screenWidth);
                handler.removeMessages(HIDE_MEDIA_CONTROLLER);
                break;
            // 手指移动
            case MotionEvent.ACTION_MOVE:
                // 移动的记录相关值
                float endY = event.getY();
                float distanceY = startY - endY;
                // 改变声音 = （滑动屏幕距离 : 总距离）* 音量最大值
                float delta = distanceY / touchRang * maxVolume;
                // 最终声音 = 原来音量 + 改变声音
                int volume = (int) Math.min(maxVolume, Math.max(0, mVol + delta));
                if (delta != 0) {
                    updateVolume(volume, isMute = false);
                }
                break;
            // 手指离开
            case MotionEvent.ACTION_UP:
                handler.sendEmptyMessageDelayed(HIDE_MEDIA_CONTROLLER, 3000);
                break;
            default:
                break;
        }
        return super.onTouchEvent(event);
    }

    /**
     * 隐藏控制面板
     */
    private void hideMediaController() {
        mediaControl.setVisibility(View.GONE);
        isShowMediaController = false;
    }

    /**
     * 显示控制面板
     */
    private void showMediaController() {
        mediaControl.setVisibility(View.VISIBLE);
        isShowMediaController = true;
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_VOLUME_DOWN) {
            currentVolume--;
            updateVolume(currentVolume, false);
            handler.removeMessages(HIDE_MEDIA_CONTROLLER);
            handler.sendEmptyMessageDelayed(HIDE_MEDIA_CONTROLLER, 3000);
            return true;
        } else if (keyCode == KeyEvent.KEYCODE_VOLUME_UP) {
            currentVolume++;
            updateVolume(currentVolume, false);
            handler.removeMessages(HIDE_MEDIA_CONTROLLER);
            handler.sendEmptyMessageDelayed(HIDE_MEDIA_CONTROLLER, 3000);
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // 横屏 或 竖屏
        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            // 隐藏最上面那一栏
            setSystemUiHide();
            // 设置为全屏
            setVideoViewScale(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
            // 设置视频尺寸
            setVideoType(DEFAULT_SCREEN);
            main_tab_layout.setVisibility(View.GONE);
            main_vp.setVisibility(View.GONE);
            mainToolBar.setVisibility(View.GONE);

            // 强制移除半屏状态
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        } else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {
            // 显示最上面那一栏
            setSystemUiShow();
            // 设置视频尺寸
            setVideoType(DEFAULT_SCREEN);
            ViewGroup.LayoutParams params = llVideo.getLayoutParams();
            params.width = ViewGroup.LayoutParams.MATCH_PARENT;
            params.height = ViewGroup.LayoutParams.MATCH_PARENT;
            llVideo.setLayoutParams(params);

            ViewGroup.LayoutParams params1 = rl.getLayoutParams();
            params1.width = ViewGroup.LayoutParams.MATCH_PARENT;
            params1.height = DensityUtils.dp2px(240);
            rl.setLayoutParams(params1);

            main_tab_layout.setVisibility(View.VISIBLE);
            main_vp.setVisibility(View.VISIBLE);
            mainToolBar.setVisibility(View.VISIBLE);

            // 强制移除全屏状态
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
        }
    }

    /**
     * 隐藏SystemUI
     */
    private void setSystemUiHide() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            View decorView = getWindow().getDecorView();
            decorView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
        }
    }

    /**
     * 显示SystemUI
     */
    private void setSystemUiShow() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            View decorView = getWindow().getDecorView();
            decorView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_VISIBLE);
        }
    }

    /**
     * 设置VideoView的大小
     */
    private void setVideoViewScale(int width, int height) {
        ViewGroup.LayoutParams params = llVideo.getLayoutParams();
        params.width = width;
        params.height = height;
        llVideo.setLayoutParams(params);

        ViewGroup.LayoutParams params1 = rl.getLayoutParams();
        params1.width = width;
        params1.height = height;
        rl.setLayoutParams(params1);
    }

    /**
     * 返回事件
     */
    @Override
    public void onBackPressed() {
        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        } else {
            super.onBackPressed();
        }
    }

    /**
     * 直播投屏
     */
    public void projectLive() {
        // String url = TvSend.buildLiveProjectScreenUrl(channelId);

        // 判断：如果有组播地址则播放组播，否则则播放HLS
        String url;
        if (multicastUrl != null && !"".equals(multicastUrl) && !"null".equals(multicastUrl)) {
            url = TvSend.buildLiveProjectScreenUrlByHls(multicastUrl);
        } else {
            url = TvSend.buildLiveProjectScreenUrlByHls(playURL);
        }

        // 发送直播投屏请求
        OkGo.<String>post(url).execute(new StringCallback() {
            @Override
            public void onSuccess(Response<String> response) {
                String data = response.body();
                LogUtils.i(data);
                if (data != null && !"".equals(data)) {
                    JSONObject object = JSON.parseObject(data);
                    String resCode = object.getString("res_code");
                    if ("0".equals(resCode)) {
                        // 视频在播放则设置为暂停,按钮设置为播放
                        if (player.getPlayWhenReady()) {
                            player.setPlayWhenReady(false);
                            btnVideoStartPause.setBackgroundResource(R.drawable.btn_video_start_selector);
                        }
                    }
                }
            }
        });
    }

}
