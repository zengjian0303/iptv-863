package org.nood.iptv.fragment;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.nood.iptv.R;
import org.nood.iptv.adapter.ChannelAdapter;
import org.nood.iptv.adapter.ProgramScheduleAdapter;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * @author Noodlewar
 * @date 2017/11/28
 */
public class PageFragment extends Fragment {

    @BindView(R.id.program_schedule_vp)
    ViewPager viewPager;
    @BindView(R.id.program_schedule_tab)
    TabLayout tabLayout;

    public static PageFragment newInstance(int size, String channelId) {
        Bundle args = new Bundle();
        args.putString("channelId", channelId);

        args.putInt("size", size);
        PageFragment fragment = new PageFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public static PageFragment newInstance(int size) {
        Bundle args = new Bundle();

        args.putInt("size", size);
        PageFragment fragment = new PageFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.program_schedule, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        String channelId = getArguments().getString("channelId");
        if (channelId != null) {
            viewPager.setAdapter(new ProgramScheduleAdapter(this.getChildFragmentManager(), channelId));
        } else {
            viewPager.setAdapter(new ChannelAdapter(this.getChildFragmentManager()));
        }

        tabLayout.setupWithViewPager(viewPager);

        if (channelId != null) {
            viewPager.setCurrentItem(3);
            TabLayout.Tab tab = tabLayout.getTabAt(3);
            if (tab != null) {
                tab.select();
            }
        }
    }

}
