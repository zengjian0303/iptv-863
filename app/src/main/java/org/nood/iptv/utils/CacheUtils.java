package org.nood.iptv.utils;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * 缓存工具类
 *
 * @author Noodlewar
 * @date 2017/5/31
 */
public class CacheUtils {

    /**
     * 保存缓存数据
     *
     * @param context
     * @param key
     * @param value
     */
    public static void putString(Context context, String key, String value) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("Noodlewar", Context.MODE_PRIVATE);
        sharedPreferences.edit().putString(key, value).apply();
    }

    /**
     * 获取缓存数据
     *
     * @param context
     * @param key
     * @return
     */
    public static String getString(Context context, String key) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("Noodlewar", Context.MODE_PRIVATE);
        return sharedPreferences.getString(key, "");
    }

}
