package org.nood.iptv.utils;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

/**
 * Created by Noodlewar on 2017/8/1.
 */
public class JsonUtils {

    /**
     * channel解析
     */
    public static String channelResultResolver(String channelJson, int position) {
        JSONObject object = JSON.parseObject(channelJson);
        JSONArray array = object.getJSONArray("categoryList");
        JSONObject o = array.getJSONObject(position);
        return o.getString("channelList");
    }

}
