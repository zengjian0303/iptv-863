package org.nood.iptv.utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * 时间处理工具类
 *
 * @author Noodlewar
 * @date 2017/7/19
 */
public class DateUtils {

    /**
     * @return 返回1是星期日、2是星期一、3是星期二、4是星期三、5是星期四、6是星期五、7是星期六
     */
    public static int getTodayOfWeek() {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        return calendar.get(Calendar.DAY_OF_WEEK);
    }

    /**
     * @param week 数字代表一周的第几天
     * @return 返回中文周几的字符串
     */
    public static String getWeekStr(int week) {
        String str;
        switch (week) {
            case 1:
                str = "周日";
                break;
            case 2:
                str = "周一";
                break;
            case 3:
                str = "周二";
                break;
            case 4:
                str = "周三";
                break;
            case 5:
                str = "周四";
                break;
            case 6:
                str = "周五";
                break;
            case 7:
                str = "周六";
                break;
            default:
                str = "周日";
                break;
        }
        return str;
    }

    /**
     * 生成一周的数组
     *
     * @return 返回一周的数组
     */
    public static String[] initWeekArr() {
        int week = getTodayOfWeek();
        String[] arr = new String[7];
        week += 3;
        for (int i = 0; i < 7; i++) {
            if (i == 3) {
                arr[3] = "今天";
            } else {
                arr[i] = getWeekStr(week % 7 + 1);
            }
            week++;
        }
        return arr;
    }

    public static int getDayofweek(String date) {
        Calendar cal = Calendar.getInstance();
        if ("".equals(date)) {
            cal.setTime(new Date(System.currentTimeMillis()));
        } else {
            cal.setTime(new Date(getDateByStr2(date).getTime()));
        }
        return cal.get(Calendar.DAY_OF_WEEK);
    }

    private static Date getDateByStr2(String dd) {
        SimpleDateFormat sd = new SimpleDateFormat("yyyy-MM-dd");
        Date date;
        try {
            date = sd.parse(dd);
        } catch (ParseException e) {
            date = null;
            e.printStackTrace();
        }
        return date;
    }

    /**
     * 截取时间格式为 hh:mm
     *
     * @param date 字符串形式的日期，如："20170712082300"
     * @return
     */
    public static String trunDate(String date) {
        String hour = date.substring(8, 10);
        String minute = date.substring(10, 12);
        return hour + ":" + minute;
    }

    /**
     * 根据指定Tab序号获取日期:日期格式为："yyyyMMdd"
     *
     * @param position
     * @return
     */
    public static String getPositionDate(int position) {
        position -= 3;
        String dateFormat = "yyyyMMdd";
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.add(Calendar.DAY_OF_MONTH, position);
        Date before = calendar.getTime();
        DateFormat format = new SimpleDateFormat(dateFormat);
        return format.format(before);
    }

    /**
     * 将字符串时间转换为int型时间
     *
     * @param timeStr
     * @return
     */
    public static int convertStringToTime(String timeStr) {
        String[] times =  timeStr.split(":");
        if (times.length == 3) {
            int hour = Integer.parseInt(times[0]);
            int minute = Integer.parseInt(times[1]);
            int second = Integer.parseInt(times[2]);
            return (hour * 3600 + minute * 60 + second) * 1000;
        } else {
            return 0;
        }

    }

}
