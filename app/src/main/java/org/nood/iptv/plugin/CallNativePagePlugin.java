package org.nood.iptv.plugin;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import org.nood.iptv.activity.LivePlayer;

import org.json.JSONArray;
import org.nood.iptv.activity.VodPlayer;

import io.dcloud.common.DHInterface.AbsMgr;
import io.dcloud.common.DHInterface.IWebview;
import io.dcloud.common.DHInterface.StandardFeature;
import io.dcloud.common.util.JSUtil;

/**
 * H5调用Android本地原生页面插件
 *
 * @author Noodlewar
 */
public class CallNativePagePlugin extends StandardFeature {

    private Context context;

    @Override
    public void init(AbsMgr absMgr, String s) {
        this.context = absMgr.getContext();
        super.init(absMgr, s);
    }

    @Override
    public String execute(IWebview iWebview, String s, String[] strings) {
        return super.execute(iWebview, s, strings);
    }

    /**
     * 打开点播播放界面
     *
     * @param pWebview
     * @param array
     * @return
     */
    public String openVodPage(IWebview pWebview, JSONArray array) {
        String mediaId = array.optString(0);
        long beginTime = array.optLong(1);

        String ReturnValue = mediaId;

        Intent intent = new Intent(pWebview.getActivity(), VodPlayer.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra("mediaId", mediaId);
        intent.putExtra("beginTime", beginTime);

        context.startActivity(intent);

        return JSUtil.wrapJsVar(ReturnValue, true);
    }

    /**
     * 打开直播播放界面
     *
     * @param pWebview
     * @param array
     * @return
     */
    public String openLivePage(IWebview pWebview, JSONArray array) {
        String playURL = array.optString(0);
        String channelId = array.optString(1);
        String title = array.optString(2);
        String bindChannel = array.optString(3);
        String multicastUrl = array.optString(4);

        String ReturnValue = playURL;

        Intent intent = new Intent(pWebview.getActivity(), LivePlayer.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra("playURL", playURL);
        intent.putExtra("channelId", channelId);
        intent.putExtra("title", title);
        intent.putExtra("bindChannel", bindChannel);
        intent.putExtra("multicastUrl", multicastUrl);

        context.startActivity(intent);

        return JSUtil.wrapJsVar(ReturnValue, true);
    }

}
